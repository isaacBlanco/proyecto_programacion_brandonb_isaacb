
package Datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Buscar_Rutas {
    
    
    public ArrayList<String> leerarchivo() {
        ArrayList<String> content = new ArrayList<>();
        try {
            File file = new File("RutasTable.txt");
            BufferedReader buffer = new BufferedReader(new FileReader(file));
            while (buffer.ready()) {
                
                content.add(buffer.readLine());
            }
            buffer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return content;
    }
    
    
    public void archivo(String data){
        try {
            File file = new File("RutasTable.txt");
            BufferedWriter buffer = new BufferedWriter(new FileWriter(file, true));
            buffer.write(data + "\r\n");
          
            buffer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    
}
