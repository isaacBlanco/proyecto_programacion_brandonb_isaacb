/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;


import Datos.BaseDatosUnidades;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Francisco
 */
public class Unidades {
    private String infoP = null;
     private final BaseDatosUnidades bd = new BaseDatosUnidades();
    private String placa;
    private String campos;
    private String nomterminal;

    public Unidades(String placa, String campos, String nomterminal) {
        this.placa = placa;
        this.campos = campos;
        this.nomterminal = nomterminal;
    }

    public Unidades() {
      
    }

    

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCampos() {
        return campos;
    }

    public void setCampos(String campos) {
        this.campos = campos;
    }

    public String getNomterminal() {
        return nomterminal;
    }

    public void setNomterminal(String nomterminal) {
        this.nomterminal = nomterminal;
    }
    
   private final BaseDatosUnidades unii = new BaseDatosUnidades();
   public void GuardarArchivo(String placa, String campos, String nomterminal) {
        String format = placa + "," + campos + "," + nomterminal;
        unii.InsertarArchivo(format);
    }
   

    public boolean validarPlaca(String idRead){
      boolean existe = false;
        ArrayList<String> data = getData();
        for (String line : data) {
            String[] parts = line.split(",");
            String id = parts[0];
            if (id.contains(idRead)) {
                id="";
                infoP = line;
                existe = true;
            }
        }
        return existe;
  }
 public ArrayList<String> getData() {
        return bd.leerarchivo();
    }
 public String getInfoP(){
        return infoP;
    }
    

    public ArrayList<String> Mostrar() {
        return unii.leerarchivo();
    }

    public DefaultTableModel modee(DefaultTableModel mod) {
        ArrayList<String> datos = Mostrar();
        if (!datos.isEmpty()) {
            mod.removeRow(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                mod.addRow(columna);
            }
        }
        return mod;
    }


    public String Mostrar(DefaultComboBoxModel model) {
        return null;
       //To change body of generated methods, choose Tools | Templates.
    
}
}
