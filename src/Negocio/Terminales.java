/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BaseDatosTerminales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


/**
 *
 * @author Francisco
 */
public class Terminales {
    private String infoP = null;
     private final BaseDatosTerminales bd = new BaseDatosTerminales();
private String id;
private String nombre;
private String lugar;
private String numeroterminal;

    public Terminales() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getNumeroterminal() {
        return numeroterminal;
    }

    public void setNumeroterminal(String numeroterminal) {
        this.numeroterminal = numeroterminal;
    }

    public Terminales(String id, String nombre, String lugar, String numeroterminal) {
        this.id = id;
        this.nombre = nombre;
        this.lugar = lugar;
        this.numeroterminal = numeroterminal;
    }
        
public void guardar(PrintWriter Escribe) {
        Escribe.println(id);
        Escribe.println(nombre);
        Escribe.println(lugar);
        Escribe.println(numeroterminal);
    }


   
    public void GuardarArchivo(String id,String nombre,String lugar,String numeroterminal) {
        String format = id + "," + nombre + "," + lugar + "," + numeroterminal;
        bd.InsertarArchivo(format);
    }
  public boolean validarDatos(String id, String nombre, String lugar, String numeroterminal) {
        boolean valid = false;
        if (!id.contains("_") && !nombre.isEmpty() && !lugar.isEmpty()&& !numeroterminal.isEmpty()) {
            valid = true;
        }
        return valid;
    }
  public boolean validarId(String idRead){
      boolean existe = false;
        ArrayList<String> data = getData();
        for (String line : data) {
            String[] parts = line.split(",");
            String id = parts[0];
            if (id.contains(idRead)) {
                id="";
                infoP = line;
                existe = true;
            }
        }
        return existe;
  }
   
 public String getInfoP(){
        return infoP;
    }
   

   

    public ArrayList<String> getData() {
        return bd.leerarchivo();
    }
   

    public ArrayList<String> Reporte() {
        return bd.leerarchivo();
    }

    public DefaultTableModel mode(DefaultTableModel mod) {
        ArrayList<String> datos = Reporte();
        if (!datos.isEmpty()) {
            mod.removeRow(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                mod.addRow(columna);
            }
        }
        return mod;
    }
     public DefaultComboBoxModel mode(DefaultComboBoxModel mod) {
        
         ArrayList<String> datos = Reporte();
        
        if (!datos.isEmpty()) {
            mod.removeElement(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                mod.addElement(columna);
            }
        }
        return mod;
    }

    public String agregarreport(DefaultComboBoxModel model) {
        return null;
       //To change body of generated methods, choose Tools | Templates.
    
}
    

   

   

    

    
    

    

}
