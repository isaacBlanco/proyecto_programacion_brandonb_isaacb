/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BaseDatosRutas;
import Datos.Buscar_Rutas;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import Presentacion.Pasajero_Principal;
import java.util.Calendar;
import java.util.List;
/**
 *
 * @author Francisco
 */
public class Rutas {
    
    
    private final Buscar_Rutas RutasReg = new Buscar_Rutas();
    private final BaseDatosRutas bd = new BaseDatosRutas();
    private String infoP = null;
    private String id;
    private String horasalida;

    public String getHorasalida() {
        return horasalida;
    }

    public void setHorasalida(String horasalida) {
        this.horasalida = horasalida;
    }

    public String getHorallegada() {
        return horallegada;
    }

    public void setHorallegada(String horallegada) {
        this.horallegada = horallegada;
    }

    private String horallegada;
    private String origen;
    private String intermedio;
    private String destino;
    private String precio;
    private String fechasalida;
    private String fechallegada;
    List<String> lista = new ArrayList<>();

    
    public Rutas(String id, String origen, String intermedio, String destino, String precio, String fechasalida, String horasalida, String fechallegada, String horallegada) {
        
        this.id = id;
        this.origen = origen;
        this.intermedio = intermedio;
        this.destino = destino;
        this.precio = precio;
        this.fechasalida = fechasalida;
        this.horasalida = horasalida;
        this.fechallegada = fechallegada;
        this.horallegada = horallegada;
       
    }
    

    public Rutas(){

}
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getIntermedio() {
        return intermedio;
    }

    public void setIntermedio(String origen) {
        this.intermedio = intermedio;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFechasalida() {
        return fechasalida;
    }

    public void setFechasalida(String fechasalida) {
        this.fechasalida = fechasalida;
    }

    public String getFechallegada() {
        return fechallegada;
    }

    public void setFechallegada(String fechallegada) {
        this.fechallegada = fechallegada;
    }

    public void GuardarArchivoRutas(String id, String origen, String intermedio, String destino,
            String precio, String fechayhoradesalida, String horasalida,
            String fechayhorallegada, String horallegada) {
        String format = id + "," + origen + "," + intermedio + "," + destino + "," + precio + "," + fechayhoradesalida + "," + horasalida + ","
                + fechayhorallegada + "," + horallegada;
        bd.InsertarArchivoTabl(format);
    }

    public void GuardarArchivo(String id, String idterminal,
            String idunidad, String precio, String fechayhoradesalida, String horasalida, String origen, String intermedio,
            String fechayhorallegada, String horallegada, String destino, String duracion) {
        String format = id + "," + idterminal + "," + idunidad + "," + precio + "," + fechayhoradesalida + "," + horasalida + ","
                + origen + "," + intermedio + "," + fechayhorallegada + "," + horallegada + "," + destino + "," + duracion;
        bd.InsertarArchivo(format);
    }

    public ArrayList<String> Reporte() {
        return bd.leerarchivoTabla();
    }

    public DefaultTableModel mode(DefaultTableModel mod) {
        ArrayList<String> datos = Reporte();
        if (!datos.isEmpty()) {
            mod.removeRow(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                mod.addRow(columna);
            }
        }
        return mod;
    }
    
    public DefaultTableModel mode1(DefaultTableModel mod) {
        ArrayList<String> datos = Reporte();
        String dia, mes, año, fecha = "";
        if (!datos.isEmpty()) {
            mod.removeRow(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                String origenT = columna[1];
                String destinoT= columna[3];
                String fecha1= columna[5];
                origen = (String) Pasajero_Principal.Combo1.getSelectedItem();
                destino = (String) Pasajero_Principal.Combo2.getSelectedItem();
                dia = Integer.toString(Pasajero_Principal.Fecha1.getCalendar().get(Calendar.DAY_OF_MONTH));
                mes = Integer.toString(Pasajero_Principal.Fecha1.getCalendar().get(Calendar.MONTH));
                año = Integer.toString(Pasajero_Principal.Fecha1.getCalendar().get(Calendar.YEAR));
                fecha = (dia + "-" + mes + "-" + año);
                if (origenT.contains(origen)&&(destinoT.contains(destino))&& (fecha1.contains(fecha))){ 
                mod.addRow(columna);
            }
        }}
        return mod;

    }
    
    
     

    public boolean validarIdRu(String idRead) {
        boolean existe = false;
        ArrayList<String> data = getData();
        for (String line : data) {
            String[] parts = line.split(",");
            String ruta = parts[0];
            if (ruta.contains(idRead)) {
                ruta = "";
                infoP = line;
                existe = true;
            }
        }
        return existe;
    }
    
   /* public void lis(){
    
for(String str : lista){
  Pasajero_Principal.panel.setText(str);   
}}*/
    
    public String leerTxt(String direccion) {
        String texto = "";
        ArrayList<String> contenido = new ArrayList<>(); 
        try {
            BufferedReader bf = new BufferedReader(new FileReader(direccion));
            String temp = "";
            String bfRead;
            ArrayList<String> data = archivo();
            
            for (String linea : data) {
                String[] posicion = linea.split(",");
                String datos= linea;
                String origenT = posicion[1];
                String destinoT= posicion[3];
                origen = (String) Pasajero_Principal.Combo1.getSelectedItem();
                destino = (String) Pasajero_Principal.Combo2.getSelectedItem();               
                if (origenT.contains(origen)&&(destinoT.contains(destino))){                    
              
                
                lista.add(linea);
                
                }
                texto= texto+lista;
                }
                

            }catch(Exception e){
        
        
        }
            return texto;
            
        }
    
   
    
    
    
    public void AlmacenarDatos(String id, String idterminal,
            String idunidad, String precio, String fechayhoradesalida, String horasalida, String origen, String intermedio,String fechayhorallegada, String horallegada, String destino, String duracion) {

        String formato = id  + idterminal +  idunidad +  precio +  fechayhoradesalida +  horasalida + origen +  intermedio + fechayhorallegada +  horallegada + destino + duracion;
        RutasReg.archivo(formato);
    }
    
    
    

    public String getInfoP() {
        return infoP;
    }

    public ArrayList<String> getData() {
        return bd.leerarchivo();
    }
    
    public ArrayList<String> archivo() {
        return RutasReg.leerarchivo();
    }

}
