/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Datos.BaseDatosTerminales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


import Negocio.Terminales;


/**
 *
 * @author Francisco
 */
public class MantenimientodeTerminales extends javax.swing.JFrame {
    //String ubicacion= System.getProperty("user.dir");
Terminales Ter = new Terminales();


BaseDatosTerminales bd= new BaseDatosTerminales();

 private final ArrayList<Terminales> Lista;

    /**
     * Creates new form MantenimientodeTerminales
     */

 
    public MantenimientodeTerminales() {
       
         Lista= new ArrayList<>();
        initComponents();
        
        
        
        Tabla();
        
       this.combo.addItem("Heredia");
       this.combo.addItem("Puntarenas");
       this.combo.addItem("San Carlos");
       this. combo.addItem("San Jose");
       this. combo.addItem("Alajuela");
       this.combo.addItem("Cartago");
       this.combo.addItem("Limón");
               
        
        
    }
private void Tabla() {
        DefaultTableModel model = (DefaultTableModel) tblTerminales.getModel();
        tblTerminales.setModel(Ter.mode(model));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        numter = new javax.swing.JComboBox<>();
        txtNombre = new javax.swing.JTextField();
        combo = new javax.swing.JComboBox<>();
        btnCrear = new javax.swing.JButton();
        txtNumeroID = new javax.swing.JTextField();
        btnGenerar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTerminales = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        btnVollver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Id Terminal:");

        jLabel2.setText("Nombre:");

        jLabel4.setText("Lugar:");

        jLabel9.setText("Numero Terminal:");

        numter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2" }));
        numter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numterActionPerformed(evt);
            }
        });

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });

        combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboActionPerformed(evt);
            }
        });

        btnCrear.setText("Crear");
        btnCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearActionPerformed(evt);
            }
        });

        txtNumeroID.setEditable(false);
        txtNumeroID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroIDActionPerformed(evt);
            }
        });

        btnGenerar.setText("GenerarID");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });

        tblTerminales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Id", "Nombre", "Lugar", "NumeroTerminal"
            }
        ));
        tblTerminales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTerminalesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblTerminales);

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jButton1.setText("Mostrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnVollver.setText("Volver");
        btnVollver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVollverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel4)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnModificar)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnEliminar))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNumeroID, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnGenerar))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(numter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnCrear)
                        .addGap(143, 143, 143)
                        .addComponent(btnVollver))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 436, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(450, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNumeroID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGenerar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(btnEliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(btnModificar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCrear)
                    .addComponent(btnVollver))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void numeroAl(){
Stack<Integer>Numeros=new Stack<Integer>();
Random rnd= new Random();
int aleatorio;
int aleatorios []=new int[15];

for (int i=0;i<15;i++){
    aleatorio=(int)(rnd.nextDouble()*15+1);
    while (Numeros.contains(aleatorio)){
         aleatorio=(int)(rnd.nextDouble()*15+1);    
    }
    Numeros.push(aleatorio);
    aleatorios[i]=aleatorio;
    txtNumeroID.setText(aleatorio+"");
}

}
 private void verDatos(){
String Mat[][]=new String[Lista.size()][4];
Terminales aux;
for (int i=0;i<Lista.size();i++){
    aux=Lista.get(i);
    Mat[i][0]=aux.getId();
    Mat[i][1]=aux.getNombre();
    Mat[i][2]=(String)(aux.getLugar());
    Mat[i][3]=(String)(aux.getNumeroterminal());

}
 tblTerminales.setModel(new javax.swing.table.DefaultTableModel(
           
            Mat,
            new String [] {
                "ID", "Nombre", "Lugar", "NumeroTerminal"
            }
        ));
}
 

    
 private void eliminar(){
 int c;
 try{
 c= tblTerminales.getSelectedRow();
 Lista.remove(c);
 Tabla();
//     String datos=null;
// bd.InsertarArchivo(datos);
 }catch (Exception e){
     JOptionPane.showMessageDialog(null,"Favor escoja una fila");
 } 
 }
 
 private void modificar(){
 int c;
 String id, nombre, lugar,numeroterminal;
 Terminales aux;
 try{
     
     c= tblTerminales.getSelectedRow();
     aux=Lista.get(c);
     id=txtNumeroID.getText();
     nombre=txtNombre.getText();
     aux.setNombre(nombre);
     lugar=(String) combo.getSelectedItem();
     aux.setLugar(lugar);
     numeroterminal=(String) numter.getSelectedItem();
     aux.setNumeroterminal(numeroterminal);
     
 }catch (Exception e ){
     JOptionPane.showMessageDialog(null,"Favor escoja una fila");
 }
 }       

    
private void limpiar(){
 txtNumeroID.setText(null);
 txtNombre.setText(null);}


    private void Crear() {
        String id, nombre,lugar,numeroterminal= "";
        try {
        id = txtNumeroID.getText();
        nombre = txtNombre.getText();
        lugar= (String) combo.getSelectedItem();
        numeroterminal= (String) numter.getSelectedItem();
        
         if (!Ter.validarId(id)){
                    Lista.add(new Terminales(id ,nombre, lugar,numeroterminal));
        
        Ter.GuardarArchivo(id,nombre,lugar,numeroterminal);
        JOptionPane.showMessageDialog(null,"Terminal correctamente registrada");
            }else {JOptionPane.showMessageDialog(null,
                        "La ID  ya existe", "ID encontrada",
                        JOptionPane.ERROR_MESSAGE);
            }
        
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verifica los datos");
        }
        
        
    }

    
                 
 
  
   
    
    private void comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboActionPerformed
  // jcombox();
    }//GEN-LAST:event_comboActionPerformed

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
   numeroAl();  
    }//GEN-LAST:event_btnGenerarActionPerformed

    private void btnCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearActionPerformed
     
        Crear(); 
        limpiar();
      verDatos();
     // MenuMantTerminales mostrar = new MenuMantTerminales();
       // mostrar.setVisible(true);
    }//GEN-LAST:event_btnCrearActionPerformed

    private void txtNumeroIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroIDActionPerformed
       // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroIDActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
eliminar(); 

verDatos();// TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        modificar();
verDatos();// TODO add your handling code here:
    }//GEN-LAST:event_btnModificarActionPerformed
        
    private void numterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numterActionPerformed
//combo2();        // TODO add your handling code here:
    }//GEN-LAST:event_numterActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing


//String id ;
//String lugar;
//    String nombre;
//    String numeroterminal;
//        String botones[] = {"Si", "No"};
//       int n = JOptionPane.showOptionDialog(null, "Desea guardar la informacio?", "Titulo", 0,0, null, botones, null);
//       if(n == 0)
//            //verDatos();
//        
//        guardaTabla( );
           //guardaTabla();
//            id = txtNumeroID.getText();
//        nombre = txtNombre.getText();
//        lugar= (String) combo.getSelectedItem();
//        numeroterminal= (String) numter.getSelectedItem();
            
            //Ter.GuardarArchivo();
              // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
Tabla();
//car();
//JFileChooser fc=new JFileChooser(); 
//    fc.showOpenDialog(null);
//    File archivo = fc.getSelectedFile();
//    try{
//        FileReader fr = new FileReader (archivo);
//        BufferedReader br = new BufferedReader(fr);
//        String Texto="";
//        String Linea= "";
//        while(((Linea=br.readLine())!= null) ) {
//            Texto+=Linea+"\n";
//        }
//        verDatos();
//        //txaTerminales.setText(Texto);
//        JOptionPane.showMessageDialog(null,"Archivo leido correctamente");}
//        catch(Exception e){
//        }        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tblTerminalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTerminalesMouseClicked
int seleccion=tblTerminales.getSelectedRow();
txtNumeroID.setText(tblTerminales.getValueAt(seleccion,0).toString());
txtNombre.setText(tblTerminales.getValueAt(seleccion,1).toString());
combo.setSelectedItem(tblTerminales.getValueAt(seleccion,2).toString());
numter.setSelectedItem(tblTerminales.getValueAt(seleccion,3).toString());
        // TODO add your handling code here:
    }//GEN-LAST:event_tblTerminalesMouseClicked

    private void btnVollverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVollverActionPerformed
       dispose();
       MenuAdministrador ventana = new MenuAdministrador();
       ventana.setVisible(true);
    }//GEN-LAST:event_btnVollverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeTerminales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeTerminales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeTerminales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeTerminales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MantenimientodeTerminales().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCrear;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGenerar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnVollver;
    private javax.swing.JComboBox<String> combo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> numter;
    private javax.swing.JTable tblTerminales;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumeroID;
    // End of variables declaration//GEN-END:variables
}
